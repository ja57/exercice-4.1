package dao;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;

public class BadgeWalletDAO {
	private String resources;

	public BadgeWalletDAO(String path) {
		this.resources = path;
	}

	public void addBadge(File image) throws IOException {

		ImageSerializer serializer = new ImageSerializerBase64Impl();

		String encodedImage = (String) serializer.serialize(image);
		Files.writeString(Paths.get(resources), encodedImage, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
	}

	public void getBadge(OutputStream memoryBadgeStream) throws IOException {
		ByteArrayOutputStream byteToConvert = (ByteArrayOutputStream) memoryBadgeStream;
		byteToConvert.writeBytes(Files.readAllBytes(Paths.get(resources)));
	}
}
